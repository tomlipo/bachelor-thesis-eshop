<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('set null');

            $table->boolean('is_active')->default(false);

            $table->timestamps();
        });

        Schema::create('category_texts', function (Blueprint $table) {
            $table->id();

            $table->foreignId('category_id')->constrained()->onUpdate('cascade')->onDelete('cascade');

            $table->string('lang_id', 2);
            $table->foreign('lang_id')->references('slug')->on('langs')->onUpdate('cascade')->onDelete('cascade');

            $table->string('name', 128)->nullable();
            $table->string('url', 128)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_texts');
        Schema::dropIfExists('categories');
    }
}
