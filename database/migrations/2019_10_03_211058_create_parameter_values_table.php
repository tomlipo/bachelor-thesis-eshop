<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_values', function (Blueprint $table) {
            $table->id();

            $table->foreignId('parameter_id')->constrained()->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('parameter_value_texts', function (Blueprint $table) {
            $table->id();

            $table->foreignId('parameter_value_id')->constrained()->onUpdate('cascade')->onDelete('cascade');

            $table->string('lang_id', 2);
            $table->foreign('lang_id')->references('slug')->on('langs')->onUpdate('cascade')->onDelete('cascade');

            $table->string('name', 128)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_value_texts');
        Schema::dropIfExists('parameter_values');
    }
}
