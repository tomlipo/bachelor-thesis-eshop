<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_types', function (Blueprint $table) {
            $table->id();

            $table->string('slug', 64)->unique();

            $table->timestamps();
        });

        Schema::create('parameter_type_texts', function (Blueprint $table) {
            $table->id();

            $table->foreignId('parameter_type_id')->constrained()->onUpdate('cascade')->onDelete('cascade');

            $table->string('lang_id', 2);
            $table->foreign('lang_id')->references('slug')->on('langs')->onUpdate('cascade')->onDelete('cascade');

            $table->string('name', 128)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_type_texts');
        Schema::dropIfExists('parameter_types');
    }
}
