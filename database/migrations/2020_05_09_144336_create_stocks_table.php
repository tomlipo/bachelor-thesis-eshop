<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->id();

            $table->boolean('is_active')->default(true);

            $table->timestamps();
        });

        Schema::create('stock_texts', function (Blueprint $table) {
            $table->id();

            $table->foreignId('stock_id')->constrained()->onUpdate('cascade')->onDelete('cascade');

            $table->string('lang_id', 2);
            $table->foreign('lang_id')->references('slug')->on('langs')->onUpdate('cascade')->onDelete('cascade');

            $table->string('name', 128)->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('stock_texts');
        Schema::dropIfExists('stocks');
    }
}
