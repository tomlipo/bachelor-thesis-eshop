<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters', function (Blueprint $table) {
            $table->id();

            $table->foreignId('parameter_type_id')->nullable()->constrained()->onUpdate('cascade')->onDelete('set null');

            $table->string('slug', 64)->unique();

            $table->timestamps();
        });

        Schema::create('parameter_texts', function (Blueprint $table) {
            $table->id();

            $table->foreignId('parameter_id')->constrained()->onUpdate('cascade')->onDelete('cascade');

            $table->string('lang_id', 2);
            $table->foreign('lang_id')->references('slug')->on('langs')->onUpdate('cascade')->onDelete('cascade');

            $table->string('name', 128)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_texts');
        Schema::dropIfExists('parameters');
    }
}
