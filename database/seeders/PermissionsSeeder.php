<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    protected $permissions = [
        'users' => ['create', 'read', 'update', 'delete'],
    ];

    public function run()
    {
        $permissions = $this->createPermissionsArray();

        Permission::insert($permissions);

        Role::where('name', 'super-admin')->first()->permissions()->sync(Permission::all());
    }

	protected function createPermissionsArray(): array
	{
        $permissions = [];

        foreach ($this->permissions as $resource => $actions) {
            foreach ($actions as $action) {
                $permissions[] = [
                    'name' => "$resource.$action"
                ];
            }
        }

        return $permissions;
	}
}
