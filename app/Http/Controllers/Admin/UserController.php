<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        return User::paginate(10);
    }

    public function create()
    {

    }

    public function store(UserRequest $request)
    {
        $user = User::create($request->validated());

        return $user;
    }

    public function edit(User $user)
    {

    }

    public function update(UserRequest $request, User $user)
    {
        $user->update($request->validated());

        return $user;

    }

    public function destroy(User $user)
    {
        $user->delete();

        return $user;
    }
}
