<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    protected function isUpdateMethod()
    {
        return $this->method() == 'PATCH' || $this->method() == 'PUT';
    }
}
