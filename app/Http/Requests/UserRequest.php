<?php

namespace App\Http\Requests;

class UserRequest extends BaseRequest
{
    public function rules()
    {
        if ($this->isUpdateMethod()) {
            return array_merge($this->createRules(), $this->updateRules());
        }

        return $this->createRules();
    }

    public function createRules()
    {
        return [
            'role_id' => 'nullable|exists:roles,id',
            'name' => 'required|max:191',
            'email' => 'email|max:191|required|unique:users,email',
            'password' => 'sometimes|max:191|string|confirmed',
        ];
    }

    public function updateRules()
    {
        return [
            'email' => 'email|max:191|required|unique:users,email,' . $this->user->id,
        ];
    }
}
